#!/usr/bin/env python

"""
Columbia's COMS W4111.003 Introduction to Databases
Example Webserver

To run locally:

    python server.py

Go to http://localhost:8111 in your browser.

A debugger such as "pdb" may be helpful for debugging.
Read about it online.
"""

import os
from sqlalchemy import *
from sqlalchemy.pool import NullPool
from flask import Flask, request, render_template, g, redirect, abort, Response

tmpl_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates')
app = Flask(__name__, template_folder=tmpl_dir)


#
# The following is a dummy URI that does not connect to a valid database.
# You will need to modify it to connect to your Part 2 database in order to use the data.
#
# XXX: The URI should be in the format of: 
#
#     postgresql://USER:PASSWORD@104.196.152.219/proj1part2
#
# For example, if you had username biliris and password foobar, then the following line would be:
#
#     DATABASEURI = "postgresql://biliris:foobar@104.196.152.219/proj1part2"
#
DATABASEURI = "postgresql://yx2584:19970520@104.196.152.219/proj1part2"


#
# This line creates a database engine that knows how to connect to the URI above.
#
engine = create_engine(DATABASEURI)

#
# Example of running queries in your database
# Note that this will probably not work if you already have a table named 'test' in your database, containing meaningful data.
# This is only an example showing you how to run queries in your database using SQLAlchemy.
#
# engine.execute("""CREATE TABLE IF NOT EXISTS test (
#   id serial,
#   name text
# );""")
# engine.execute("""INSERT INTO test(name) VALUES ('grace hopper'), ('alan turing'), ('ada lovelace');""")


@app.before_request
def before_request():
  """
  This function is run at the beginning of every web request 
  (every time you enter an address in the web browser).
  We use it to setup a database connection that can be used throughout the request.

  The variable g is globally accessible.
  """
  try:
    g.conn = engine.connect()
  except:
    print("uh oh, problem connecting to database")
    import traceback; traceback.print_exc()
    g.conn = None

@app.teardown_request
def teardown_request(exception):
  """
  At the end of the web request, this makes sure to close the database connection.
  If you don't, the database could run out of memory!
  """
  try:
    g.conn.close()
  except Exception as e:
    pass


#
# @app.route is a decorator around index() that means:
#   run index() whenever the user tries to access the "/" path using a GET request
#
# If you wanted the user to go to, for example, localhost:8111/foobar/ with POST or GET then you could use:
#
#       @app.route("/foobar/", methods=["POST", "GET"])
#
# PROTIP: (the trailing / in the path is important)
# 
# see for routing: http://flask.pocoo.org/docs/0.10/quickstart/#routing
# see for decorators: http://simeonfranklin.com/blog/2012/jul/1/python-decorators-in-12-steps/
#
@app.route('/')
def index():
  """
  request is a special object that Flask provides to access web request information:

  request.method:   "GET" or "POST"
  request.form:     if the browser submitted a form, this contains the data in the form
  request.args:     dictionary of URL arguments, e.g., {a:1, b:2} for http://localhost?a=1&b=2

  See its API: http://flask.pocoo.org/docs/0.10/api/#incoming-request-data
  """

  # DEBUG: this is debugging code to see what request looks like
  # print(request.args)


  #
  # example of a database query
  #
  cursor = g.conn.execute("SELECT club_name FROM clubs")
  outputs = []
  for result in cursor:
    outputs.append(result[0])  # can also be accessed using result[0]
  cursor.close()

  another_cursor = g.conn.execute("SELECT COUNT(*) FROM players")
  for k in another_cursor:
    count = k[0]
  another_cursor.close()

  #
  # Flask uses Jinja templates, which is an extension to HTML where you can
  # pass data to a template and dynamically generate HTML based on the data
  # (you can think of it as simple PHP)
  # documentation: https://realpython.com/blog/python/primer-on-jinja-templating/
  #
  # You can see an example template in templates/index.html
  #
  # context are the variables that are passed to the template.
  # for example, "data" key in the context variable defined below will be 
  # accessible as a variable in index.html:
  #
  #     # will print: [u'grace hopper', u'alan turing', u'ada lovelace']
  #     <div>{{data}}</div>
  #     
  #     # creates a <div> tag for each element in data
  #     # will print: 
  #     #
  #     #   <div>grace hopper</div>
  #     #   <div>alan turing</div>
  #     #   <div>ada lovelace</div>
  #     #
  #     {% for n in data %}
  #     <div>{{n}}</div>
  #     {% endfor %}
  #
  context = dict(data = outputs)


  #
  # render_template looks in the templates/ folder for files.
  # for example, the below file reads template/index.html
  #
  return render_template("index.html", **context, counts=count)

#
# This is an example of a different path.  You can see it at:
# 
#     localhost:8111/another
#
# Notice that the function name is another() rather than index()
# The functions for each app.route need to have different names
#
@app.route('/Clubs')
def Clubs():

  cursor = g.conn.execute("SELECT * FROM clubs")
  clubs = []
  for record in cursor:
    clubs.append(record)
  cursor.close()

  # context = dict(data=clubs, docs=documents)

  documents = {}
  for club in clubs:
    players = []
    cursor = g.conn.execute("SELECT * FROM players WHERE players.club_id='{}'".format(club[0]))
    for p in cursor:
      players.append(p)
    cursor.close()
    documents[club[1]] = players

  context = dict(data=clubs, docs=documents)

  return render_template("Clubs.html", **context)

@app.route('/Players')
def Players():

  cursor = g.conn.execute("SELECT * FROM players P JOIN clubs C ON P.club_id=C.club_id ORDER BY P.p_id")
  players = []
  for record in cursor:
    players.append(record)
  cursor.close()

  context = dict(data=players)

  return render_template("Players.html", **context)

@app.route('/Stadiums')
def Stadiums():

  cursor = g.conn.execute("SELECT * FROM stadiums")
  stadiums = []
  for record in cursor:
    stadiums.append(record)
  cursor.close()

  context = dict(data=stadiums)

  return render_template("Stadiums.html", **context)

@app.route('/Coaches')
def Coaches():

  cursor = g.conn.execute("SELECT * FROM (coaches CO JOIN manages M ON CO.coach_id=M.coach_id) AS A JOIN clubs C ON  A.club_id=C.club_id")
  coaches = []
  for record in cursor:
    coaches.append(record)
  cursor.close()

  cursor2 = g.conn.execute("SELECT CO.coach_fn, CO.coach_ln "
                           "FROM coaches AS CO JOIN manages M ON CO.coach_id = M.coach_id "
                           "WHERE M.club_id = (SELECT T.club_id "
                                              "FROM (SELECT C.club_id, COUNT(DISTINCT p.nationality) AS nums "
                                                    "FROM players P JOIN clubs C ON P.club_id = C.club_id GROUP BY C.club_id) AS T "
                                                    "WHERE T.nums = (SELECT MAX(T.nums) "
                                                                    "FROM (SELECT C.club_id, COUNT(DISTINCT p.nationality) AS nums "
                                                                          "FROM players P JOIN clubs C ON P.club_id = C.club_id "
                                                                          "GROUP BY C.club_id) AS T))")
  di_coach = []
  for record in cursor2:
    di_coach.append(record)
  cursor2.close()

  context = dict(data=coaches, di=di_coach)

  return render_template("Coaches.html", **context)

@app.route('/Stats')
def Stats():

  cursor = g.conn.execute("SELECT * FROM matches ORDER BY matches.date DESC")
  stats = []
  for record in cursor:
    record = list(record)
    match_id = record[0]
    goals = []
    punishments = []
    goal_cursor = g.conn.execute("SELECT P.p_fn, P.p_ln, G.numbers FROM goals G JOIN players P ON G.s_id=P.p_id WHERE G.match_id={}".format(match_id))
    for l in goal_cursor:
      goals.append(l)
    goal_cursor.close()
    record.append(goals)
    punish_cursor = g.conn.execute("SELECT P.p_fn, P.p_ln, PU.type FROM punishments PU JOIN players P ON PU.pp_id=P.p_id WHERE PU.match_id={}".format(match_id))
    for k in punish_cursor:
      punishments.append(k)
    punish_cursor.close()
    record.append(punishments)
    stats.append(record)
  cursor.close()

  context = dict(data=stats)

  return render_template("Stats.html", **context)

@app.route('/LIV')
def LIV():
  cursor = g.conn.execute("SELECT * FROM players WHERE club_id='LIV'")
  players = []
  for record in cursor:
    players.append(record)
  cursor.close()

  coach = []
  cursor2 = g.conn.execute("SELECT * FROM coaches CO JOIN manages M ON CO.coach_id=M.coach_id WHERE M.club_id='LIV'")
  for record in cursor2:
    coach.append(record)
  cursor2.close()

  count = len(players)

  context = dict(players=players, count=count, coach=coach)

  return render_template("LIV.html", **context)

@app.route('/MCI')
def MCI():
  cursor = g.conn.execute("SELECT * FROM players WHERE club_id='MCI'")
  players = []
  for record in cursor:
    players.append(record)
  cursor.close()

  coach = []
  cursor2 = g.conn.execute("SELECT * FROM coaches CO JOIN manages M ON CO.coach_id=M.coach_id WHERE M.club_id='MCI'")
  for record in cursor2:
    coach.append(record)
  cursor2.close()

  count = len(players)

  context = dict(players=players, count=count, coach=coach)

  return render_template("MCI.html", **context)

@app.route('/MUN')
def MUN():
  cursor = g.conn.execute("SELECT * FROM players WHERE club_id='MUN'")
  players = []
  for record in cursor:
    players.append(record)
  cursor.close()

  coach = []
  cursor2 = g.conn.execute("SELECT * FROM coaches CO JOIN manages M ON CO.coach_id=M.coach_id WHERE M.club_id='MUN'")
  for record in cursor2:
    coach.append(record)
  cursor2.close()

  count = len(players)

  context = dict(players=players, count=count, coach=coach)

  return render_template("MUN.html", **context)

@app.route('/CHE')
def CHE():
  cursor = g.conn.execute("SELECT * FROM players WHERE club_id='CHE'")
  players = []
  for record in cursor:
    players.append(record)
  cursor.close()

  coach = []
  cursor2 = g.conn.execute("SELECT * FROM coaches CO JOIN manages M ON CO.coach_id=M.coach_id WHERE M.club_id='CHE'")
  for record in cursor2:
    coach.append(record)
  cursor2.close()

  count = len(players)

  context = dict(players=players, count=count, coach=coach)

  return render_template("CHE.html", **context)

@app.route('/LEI')
def LEI():
  cursor = g.conn.execute("SELECT * FROM players WHERE club_id='LEI'")
  players = []
  for record in cursor:
    players.append(record)
  cursor.close()

  coach = []
  cursor2 = g.conn.execute("SELECT * FROM coaches CO JOIN manages M ON CO.coach_id=M.coach_id WHERE M.club_id='LEI'")
  for record in cursor2:
    coach.append(record)
  cursor2.close()

  count = len(players)

  context = dict(players=players, count=count, coach=coach)

  return render_template("LEI.html", **context)

@app.route('/TOT')
def TOT():
  cursor = g.conn.execute("SELECT * FROM players WHERE club_id='TOT'")
  players = []
  for record in cursor:
    players.append(record)
  cursor.close()

  coach = []
  cursor2 = g.conn.execute("SELECT * FROM coaches CO JOIN manages M ON CO.coach_id=M.coach_id WHERE M.club_id='TOT'")
  for record in cursor2:
    coach.append(record)
  cursor2.close()

  count = len(players)

  context = dict(players=players, count=count, coach=coach)

  return render_template("TOT.html", **context)

@app.route('/ARS')
def ARS():
  cursor = g.conn.execute("SELECT * FROM players WHERE club_id='ARS'")
  players = []
  for record in cursor:
    players.append(record)
  cursor.close()

  coach = []
  cursor2 = g.conn.execute("SELECT * FROM coaches CO JOIN manages M ON CO.coach_id=M.coach_id WHERE M.club_id='ARS'")
  for record in cursor2:
    coach.append(record)
  cursor2.close()

  count = len(players)

  context = dict(players=players, count=count, coach=coach)

  return render_template("ARS.html", **context)

@app.route('/SOU')
def SOU():
  cursor = g.conn.execute("SELECT * FROM players WHERE club_id='SOU'")
  players = []
  for record in cursor:
    players.append(record)
  cursor.close()

  coach = []
  cursor2 = g.conn.execute("SELECT * FROM coaches CO JOIN manages M ON CO.coach_id=M.coach_id WHERE M.club_id='SOU'")
  for record in cursor2:
    coach.append(record)
  cursor2.close()

  count = len(players)

  context = dict(players=players, count=count, coach=coach)

  return render_template("SOU.html", **context)

@app.route('/EVE')
def EVE():
  cursor = g.conn.execute("SELECT * FROM players WHERE club_id='EVE'")
  players = []
  for record in cursor:
    players.append(record)
  cursor.close()

  coach = []
  cursor2 = g.conn.execute("SELECT * FROM coaches CO JOIN manages M ON CO.coach_id=M.coach_id WHERE M.club_id='EVE'")
  for record in cursor2:
    coach.append(record)
  cursor2.close()

  count = len(players)

  context = dict(players=players, count=count, coach=coach)

  return render_template("EVE.html", **context)

@app.route('/NEW')
def NEW():
  cursor = g.conn.execute("SELECT * FROM players WHERE club_id='NEW'")
  players = []
  for record in cursor:
    players.append(record)
  cursor.close()

  coach = []
  cursor2 = g.conn.execute("SELECT * FROM coaches CO JOIN manages M ON CO.coach_id=M.coach_id WHERE M.club_id='NEW'")
  for record in cursor2:
    coach.append(record)
  cursor2.close()

  count = len(players)

  context = dict(players=players, count=count, coach=coach)

  return render_template("NEW.html", **context)

@app.route('/ClubRanks')
def ClubRanks():

  cursor = g.conn.execute("SELECT C.club_id, C.club_name, T.tot_goals "
                          "FROM clubs C JOIN (SELECT P.club_id, SUM(nums) AS tot_goals "
                                              "FROM players P JOIN (SELECT s_id, SUM(numbers) AS nums "
                                                                    "FROM goals GROUP BY s_id) AS G "
                                                              "ON P.p_id = G.s_id GROUP BY P.club_id) AS T "
                                       "ON C.club_id = T.club_id ORDER BY tot_goals DESC;")
  ranks = []

  for record in cursor:
    ranks.append(record)
  cursor.close()

  context = dict(data=ranks)

  return render_template("ClubRanks.html", **context)

@app.route('/PlayerRanks')
def PlayerRanks():

  cursor = g.conn.execute("SELECT P.p_id, P.p_fn, P.p_ln, COUNT(*) "
                          "FROM players P JOIN goals G ON P.p_id=G.s_id GROUP BY P.p_id, P.p_fn, P.p_ln ORDER BY COUNT(*) DESC;")
  ranks = []

  for record in cursor:
    ranks.append(record)
  cursor.close()

  context = dict(data=ranks)

  return render_template("PlayerRanks.html", **context)

@app.route('/add_player', methods=['POST'])
def add_player():
  arg0 = request.form['pid']
  arg1 = request.form['pfn']
  arg2 = request.form['pln']
  arg3 = request.form['position']
  arg4 = request.form['birthdate']
  arg5 = request.form['nationality']
  arg6 = request.form['age']
  arg7 = request.form['cid']

  try:
    query = "INSERT INTO players VALUES('{}', '{}', '{}', '{}', '{}', '{}', {}, '{}')"\
      .format(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7)
    g.conn.execute(query)
  except Exception as error:
    error_message = str(error.orig) + \
                    '\n Please click the back in your browser to fix the unexpected input indicated above!'
    abort(Response(error_message))

  return redirect('/Players')

@app.route('/update_player', methods=['POST'])
def update_player():
  pid = request.form['upid']

  labels = ['upfn', 'upln', 'uposition', 'ubirthdate', 'unationality', 'uage', 'ucid']
  y = ['p_fn', 'p_ln', 'position', 'birthdate', 'nationality', 'age', 'club_id']

  for i in range(len(labels)):
    arg = request.form[labels[i]]
    if len(arg) != 0:
      try:
        g.conn.execute("UPDATE players SET {}='{}' WHERE p_id='{}'".format(y[i], arg, pid))
      except Exception as error:
        error_message = str(error.orig) + \
                        '\n Please click the back in your browser to fix the unexpected input indicated above!'
        abort(Response(error_message))

  return redirect('/Players')

@app.route('/delete_player', methods=['POST'])
def delete_player():
  p_id = request.form['player id']
  try:
    g.conn.execute("DELETE FROM players WHERE players.p_id='{}'".format(p_id))
  except Exception as error:
    error_message = str(error.orig) + \
                    '\n Please click the back in your browser to fix the unexpected input indicated above!'
    abort(Response(error_message))
  return redirect('/Players')


@app.route('/login')
def login():
    abort(401)
    this_is_never_executed()

# @app.errorhandler(werkzeug.exceptions.BadRequest)
# def handle_bad_request(e):
#     return 'bad request!', 400


if __name__ == "__main__":
  import click

  @click.command()
  @click.option('--debug', is_flag=True)
  @click.option('--threaded', is_flag=True)
  @click.argument('HOST', default='0.0.0.0')
  @click.argument('PORT', default=8111, type=int)
  def run(debug, threaded, host, port):
    """
    This function handles command line parameters.
    Run the server using:

        python server.py

    Show the help text using:

        python server.py --help

    """

    HOST, PORT = host, port
    print("running on %s:%d" % (HOST, PORT))
    app.run(host=HOST, port=PORT, debug=debug, threaded=threaded)


  run()
